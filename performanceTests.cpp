/*
 * performanceTests.cpp
 *
 *  Created on: Sep 16, 2016
 *      Author: mkielanx
 */

#include <iostream>
#include <gtest/gtest.h>
#include <random>
#include <algorithm>
#include <functional>
#include <memory>
#include <chrono>
#include <tuple>
#include "matrix.hpp"

using MatrixDataType = int;

static constexpr std::size_t j[]
{ 100, 200, 300, 400, 500, 600, 700, 800, 1000, 1100, 1200, 1300, 1400, 1500, 1600};

// static constexpr std::size_t j[]
// { 100, 200, 300, 400, 500, 600, 700, 800, 1000, 1100, 1200, 1300, 1400, 1500, 1600};


constexpr std::size_t col = 500;
static constexpr std::size_t i[]
{
  col, col, col, col, col, col, col, col, col, col, col, col, col, col, col
};

// static constexpr std::size_t j[]
// { 2*i[0], 2*i[1], 2*i[2], 2*i[3], 2*i[4], 2*i[5], 2*i[6], 2*i[7], 
//   2*i[8], 2*i[9], 2*i[10], 2*i[11], 2*i[12], 2*i[13], 2*i[14] };

static Matrix<MatrixDataType, i[0], j[0]> A0, B0;
static Matrix<MatrixDataType, i[1], j[1]> A1, B1;
static Matrix<MatrixDataType, i[2], j[2]> A2, B2;
static Matrix<MatrixDataType, i[3], j[3]> A3, B3;
static Matrix<MatrixDataType, i[4], j[4]> A4, B4;
static Matrix<MatrixDataType, i[5], j[5]> A5, B5;
static Matrix<MatrixDataType, i[6], j[6]> A6, B6;
static Matrix<MatrixDataType, i[7], j[7]> A7, B7;
static Matrix<MatrixDataType, i[8], j[8]> A8, B8;
static Matrix<MatrixDataType, i[9], j[9]> A9, B9;
static Matrix<MatrixDataType, i[10], j[10]> A10, B10;

static Matrix<MatrixDataType, i[11], j[11]> A11, B11;
static Matrix<MatrixDataType, i[12], j[12]> A12, B12;
static Matrix<MatrixDataType, i[13], j[13]> A13, B13;
static Matrix<MatrixDataType, i[14], j[14]> A14, B14;

class MatrixPerformanceTest : public ::testing::Test
{
  protected:

    void SetUp() override
    {
      fillRand(A0);  fillRand(B0);
      fillRand(A1);  fillRand(B1);
      fillRand(A2);  fillRand(B2);
      fillRand(A3);  fillRand(B3);
      fillRand(A4);  fillRand(B4);
      fillRand(A5);  fillRand(B5);
      fillRand(A6);  fillRand(B6);
      fillRand(A7);  fillRand(B7);
      fillRand(A8);  fillRand(B8);
      fillRand(A9);  fillRand(B9);
      fillRand(A10);  fillRand(B10);
      fillRand(A11);  fillRand(B11);
      fillRand(A12);  fillRand(B12);
      fillRand(A13);  fillRand(B13);
      fillRand(A14);  fillRand(B14);
    }

    void TearDown() override {}

    template<typename T, std::size_t I, std::size_t J>
    void print(const Matrix<T, I, J>& A)
    {
      for (std::size_t i = 0; i < I; i++)
      {
        for (std::size_t j = 0; j < J; j++)
        {
          std::cout << A[i][j] << " ";
        }
        std::cout << std::endl;
      }
    }

    template<typename T, std::size_t i, std::size_t j>
    void fillRand(Matrix<T, i, j>& A)
    {
      std::default_random_engine dre{std::random_device{}()};
      std::uniform_int_distribution<T> ud;

      for(auto& a : A)
      {
        a.fill(ud(dre));
      }
    }


};


TEST_F(MatrixPerformanceTest, foll_row_col_time)
{
  std::cout << "size" << '\t' << "time[ms]" << std::endl;

  auto f = [](auto& A, auto& B, std::size_t i,  std::size_t j)
  {
    MatrixDataType sum = 0;
    auto begin = std::chrono::steady_clock::now();
    for(std::size_t it = 0; it<i; it++)
    {
      for(std::size_t jt = 0; jt<j; jt++)
      { 
        sum += A[it][jt];
      }
    }
    auto end = std::chrono::steady_clock::now();

    std::cout
    //<< i
    //<< '\t'
    << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count()
    << std::endl;
  };

  f(A0, B0, i[0], j[0]);
  f(A1, B1, i[1], j[1]);
  f(A2, B2, i[2], j[2]);
  f(A3, B3, i[3], j[3]);
  f(A4, B4, i[4], j[4]);
  f(A5, B5, i[5], j[5]);
  f(A6, B6, i[6], j[6]);
  f(A7, B7, i[7], j[7]);
  f(A8, B8, i[8], j[8]);
  f(A9, B9, i[9], j[9]);
  f(A10, B10, i[10], j[10]);
  f(A11, B11, i[11], j[11]);
  f(A12, B12, i[12], j[12]);
  f(A13, B13, i[13], j[13]);
  f(A14, B14, i[14], j[14]);
}


TEST_F(MatrixPerformanceTest, foll_col_row_time)
{
  std::cout << "size" << '\t' << "time[ms]" << std::endl;

  auto f = [](auto& A, auto& B, std::size_t i,  std::size_t j)
  {
    MatrixDataType sum = 0;
    auto begin = std::chrono::steady_clock::now();
    for(std::size_t jt = 0; jt<j; jt++)
    {
      for(std::size_t it = 0; it<i; it++)
      { 
        sum += A[it][jt];
      }
    }
    auto end = std::chrono::steady_clock::now();

    std::cout
   // << i
   // << '\t'
    << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count()
    << std::endl;
  };

  f(A0, B0, i[0], j[0]);
  f(A1, B1, i[1], j[1]);
  f(A2, B2, i[2], j[2]);
  f(A3, B3, i[3], j[3]);
  f(A4, B4, i[4], j[4]);
  f(A5, B5, i[5], j[5]);
  f(A6, B6, i[6], j[6]);
  f(A7, B7, i[7], j[7]);
  f(A8, B8, i[8], j[8]);
  f(A9, B9, i[9], j[9]);
  f(A10, B10, i[10], j[10]);
  f(A11, B11, i[11], j[11]);
  f(A12, B12, i[12], j[12]);
  f(A13, B13, i[13], j[13]);
  f(A14, B14, i[14], j[14]);
}



// TEST_F(MatrixPerformanceTest, multiply_time)
// {
//   std::cout << "size" << '\t' << "time[ms]" << std::endl;

//   auto f = [](auto& A, auto& B, int i)
//   {
//     auto begin = std::chrono::steady_clock::now();
//    // auto C = A * B;
//     auto end = std::chrono::steady_clock::now();
//    // (void)C;
//     std::cout
//     << i
//     << '\t'
//     << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count()
//     << std::endl;
//   };

//   f(A0, B0, i[0]);
//   f(A1, B1, i[1]);
//   f(A2, B2, i[2]);
//   f(A3, B3, i[3]);
//   f(A4, B4, i[4]);
//   f(A5, B5, i[5]);
//   f(A6, B6, i[6]);
//   f(A7, B7, i[7]);
//   f(A8, B8, i[8]);
//   f(A9, B9, i[9]);
//   f(A10, B10, i[10]);
//   f(A11, B11, i[11]);
//   f(A12, B12, i[12]);
//   f(A13, B13, i[13]);
//   f(A14, B14, i[14]);
// }


// TEST_F(MatrixPerformanceTest, LU_time)
// {
//   std::cout << "size" << '\t' << "time[ms]" << std::endl;

//   auto f = [](auto& A, int i)
//   {
//     auto begin = std::chrono::steady_clock::now();
//     //auto C = lu(A);
//     auto end = std::chrono::steady_clock::now();
//    // (void)C;

//     std::cout
//     << i
//     << '\t'
//     << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count()
//     << std::endl;
//   };

//   f(A0, i[0]);
//   f(A1, i[1]);
//   f(A2, i[2]);
//   f(A3, i[3]);
//   f(A4, i[4]);
//   f(A5, i[5]);
//   f(A6, i[6]);
//   f(A7, i[7]);
//   f(A8, i[8]);
// }


int main(int argc, char* argv[])
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
